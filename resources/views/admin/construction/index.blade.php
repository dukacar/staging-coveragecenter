@extends('admin.layouts.app')

@section('content')
<div id="page-inner">
    <div class="row">
        <div class="col-lg-12">
            <h2>Constuction leads</h2>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-lg-12">
            <div class="form-container" style="float: left">
                <form class="form-inline" method="GET">
                    <div class="form-group">
                        <input type="text" class="form-control" id="search" name="search" placeholder="Legal Name" value="{{$search ?? ''}}">
                    </div>
                    <button type="submit" class="btn btn-default">Filter</button>
                </form>
            </div>
            <div class="form-group" style="float: right">
                <a href="{{ route('constructions.create') }}" class="btn btn-primary">Create Construction Lead</a>
                <a href="{{ route('constructions.import') }}" class="btn btn-success">Import Construction Leads</a>
            </div>
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>@sortablelink('id', 'ID')</th>
                        <th>@sortablelink('license_number', 'Licence Number')</th>
                        <th>@sortablelink('legal_name', 'Legal Name')</th>
                        <th>@sortablelink('email_address', 'Email')</th>
                        <th>@sortablelink('phone', 'Phone Number')</th>
                        <th>@sortablelink('state', 'State')</th>
                        <th>@sortablelink('created_at', 'Created')</th>
                        <th>@sortablelink('updated_at', 'Updated')</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($constructions as $construction)
                    <tr>
                        <td>{{ $construction->id ?? 'n/a' }}</td>
                        <td>{{ $construction->license_number ?? 'n/a' }}</td>
                        <td>{{ $construction->legal_name ?? 'n/a' }}</td>
                        <td>{{ $construction->email_address ?? 'n/a' }}</td>
                        <td>{{ $construction->phone ?? 'n/a' }}</td>
                        <td>{{ $construction->phy_state ?? 'n/a' }}</td>
                        <td>{{ $construction->created_at ?? 'n/a' }}</td>
                        <td>{{ $construction->updated_at ?? 'n/a' }}</td>
                        <td>
                            <form action="{{ route('constructions.destroy', $construction->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a class="btn btn-primary" href="{{ route('constructions.edit', $construction->id) }}">Edit</a>
                                <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this construction lead?')">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $constructions->appends(Request::except('page'))->render() !!}
        </div>
    </div>
</div>
@endsection