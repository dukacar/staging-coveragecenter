@extends('admin.layouts.app')

@section('content')
<div id="page-inner">
    <div class="row">
        <div class="col-lg-12">
            <h2>Import construction leads from file</h2>
        </div>
    </div>
    <hr />
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {!! session('status') !!}
                    </div>
                @endif

                @if (isset($errors) && $errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    </div>
                @endif

                {{-- @if (session()->has('failures'))
                    <table class="table table-danger">
                        <tr>
                            <th>Row</th>
                            <th>Attribute</th>
                            <th>Errors</th>
                            <th>Value</th>
                        </tr>
                        @foreach (session()->get('failures') as $validation)
                            <tr>
                                <td>{{ $validation->row() }}</td>
                                <td>{{ $validation->attribute() }}</td>
                                <td>
                                    <ul>
                                        @foreach ($validation->errors() as $e)
                                            <li>{{ $e }}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>{{ $validation->values()[$validation->attribute()] }}</td>
                            </tr>
                        @endforeach
                    </table>
                @endif --}}

                <div class="card-header">
                    Please upload CSV file(s) or ZIP packed CSV file(s):
                </div>
                <div class="card-body">
                
                
                    
                    <div class='content'>
                        <!-- Dropzone -->
                        <form action="{{route('import-constructions')}}" class='dropzone' method="POST" style="width: 600px;" >
                        </form> 
                    </div>
                    
                    <script>
                        var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");

                        Dropzone.autoDiscover = false;
                        var myDropzone = new Dropzone(".dropzone",{ 
                            maxFilesize: 512,  // mb
                            acceptedFiles: ".csv,.zip,.sql",
                            // maxFiles: 1,
                        });
                        myDropzone.on("sending", function(file, xhr, formData) {
                            formData.append("_token", CSRF_TOKEN);
                        });
                        myDropzone.on("queuecomplete", function(file) {
                            // Enable submit button
                            $("#submit_import").removeClass("disabled");
                        });
                        myDropzone.on("error", function(file, errorMessage) {
                            alert(errorMessage);
                            //myDropzone.removeFile(file);
                        });
                        /*
                        myDropzone.on("success", function(file, response, action) {
                            // PHP server response
alert(response);
                            if(response == 'success') // Validate whatever you send from the server
                            {
                                this.defaultOptions.success(file);
                            }
                            else
                            {
alert('An error occurred!');
                                this.defaultOptions.error(file, 'An error occurred!');  
                            }
                        });
                        */
                    </script>
                    
                    <form id="fileUploadForm" action="{{ route('import-constructions') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-check">
                            <div class="form-group" id="progress_container" style="display:none;">
                                <div>Please don't refresh page, wait for the process to finish<span id="global_time_passed"></span>.</div>
                                <div>
                                    <span id="progress_precentage"></span><span id="time_passed"></span><span id="tries"></span>
                                </div>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                                </div>
                            </div>
                            <input name="empty_records" class="form-check-input" type="checkbox" value="1" id="empty_records">
                            <label class="form-check-label" for="empty_records">Delete all construction leads before importing?</label>
                        </div>
                        <button type="submit" id="submit_import" class="btn btn-success disabled" value="submit">Import Construction Leads</button>
                        <input type="hidden" name="submited" value="1" />
                    </form>
                    
                    <script type="text/javascript">
                        $(document).ready(function() {
                            
                            $(".progress").width($(".dropzone").width() + 42);
                            
                            $("#submit_import").on("click", function() {
                                event.preventDefault();
                                
                                // Check if empty_records is checked
                                if ($("#empty_records").is(':checked'))
                                {
                                    $('#deleteModal').modal('show');
                                }
                                else
                                {
                                    $("#fileUploadForm").submit();
                                }
                                
                                // console.log(myDropzone.getAcceptedFiles().length);
                            });
                            
                            // Confirming lead table truncation
                            $("#process").on("click", function() {
                                $('#deleteModal').modal('hide');
                                $("#fileUploadForm").submit();
                            });
                            
                            $("#fileUploadForm").on("submit", function() {
                                
                                // Disable submit button
                                $("#submit_import").addClass("disabled");
                                // Disable checkbox
                                $("#empty_records").addClass("disabled");
                                
                                $('#progress_container').show();
                                
                                var global_start_time = parseInt(Date.now() / 1000);
                                var start_time = global_start_time;
                                var percentage = '0';
                                var tries = 0;
                                
                                $('#time_passed').hide();
                                
                                // Grtting progress of processing
                                setInterval(function() {
                                    // Time passed update
                                    const global_time_passed = ' (' + (parseInt(Date.now() / 1000) - global_start_time) + ' sec.)';
                                    $('#global_time_passed').html(global_time_passed);

                                    // Get progress file contents
                                    $.getJSON('/admin/import-constructions/progress', function(data) {
                                        if(data[0] !== '' && data[0] !== 0) {
                                            const message = JSON.parse(data[0]);
                                            const time_passed = parseInt(Date.now() / 1000) - parseInt(message.start_time) + ' sec.)';
                                            $('#time_passed').html(time_passed);
                                            
                                            if (message.message.startsWith("Preparing data from file"))
                                            {
                                                tries++;
                                                $('#progress_precentage').html(message.message + ' (');
                                                $('#tries').html('<b>.</b>'.repeat(tries));
                                                $('#time_passed').show();
                                            }
                                            else
                                            {
                                                tries = 0;
                                                $('#tries').html("");
                                                const html_message = 'Processing file: <b>' + message.file_name + '</b>, rows: <b>' + message.row_number + '</b> of <b>' + message.lines_count + '</b> (<b>' + message.percentage + '%</b>, ';
                                                $('#progress_precentage').html(html_message);
                                                $('#time_passed').show();
                                            }
                                            $('.progress .progress-bar').css("width", message.percentage+'%', function() {
                                                return $(this).attr("aria-valuenow", message.percentage) + "%";
                                            });
                                        }
                                    });
                                }, 1000);

                                $.post(
                                    $(this).prop('action'),
                                    {"_token": $(this).find('input[name=_token]').val()},
                                    function() {
                                        window.location.href = 'success';
                                    },
                                    'json'
                                );
                                

                                // return false;
                            });
                        });
                    </script>
                    
                    <!-- End Delete Modal -->
                    <div class="modal fade text-xs-left" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel2"><i class="icon-warning2"></i>Delete all construction leads before importing?</h4>
                                </div>
                                <div class="modal-body">
                                    <p>All construction lead records will be deleted from the database before import starts.<br />Are you sure?</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">No, don't proceed</button>
                                    <button type="button" id="process" class="btn btn-danger delete">Proceed and delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <!-- 
                    <form id="fileUploadForm" action="{{ route('import-constructions') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <input type="file" name="file" />
                        </div>
                        <div class="form-group">
                            <div class="progress" style="width: 200px; visibility:hidden;">
                                <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success">Import Construction Lads</button>
                    </form>
                    <script>
                        $(function () {
                            $(document).ready(function () {
                                $('#fileUploadForm').ajaxForm({
                                    beforeSend: function () {
                                        var percentage = '0';
                                        $('.progress').css("visibility", "visible");
                                    },
                                    uploadProgress: function (event, position, total, percentComplete) {
                                        var percentage = percentComplete;
                                        $('.progress .progress-bar').css("width", percentage+'%', function() {
                                          return $(this).attr("aria-valuenow", percentage) + "%";
                                        })
                                    },
                                    complete: function (xhr) {
                                        // console.log('File uploaded!');
                                    }
                                });
                            });
                        });
                    </script>
                    -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection