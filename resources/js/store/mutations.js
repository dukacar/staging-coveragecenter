let mutations = {
    // lead
    UPDATE_LEAD(state, lead) {
        state.isLoading = true;
    },
    UPDATE_LEAD_SUCCESS(state) {
        state.errors = {};
        state.isLoading = false;
    },
    UPDATE_LEAD_ERROR(state, errors) {          
        state.errors = errors.errors;
        state.isLoading = false;
    },
    FETCH_LEAD(state, lead) {
        state.isLoading = true;
        state.errors = {};
    },
    FETCH_LEAD_SUCCESS(state, lead) {
        state.lead = lead;
        state.isLoading = false;
    },
    FETCH_LEAD_ERROR(state, errors) {
        state.errors = errors.errors;
        state.isLoading = false;
    },
    DELETE_LEAD(state, lead) {
        let index = state.lead.findIndex(item => item.id === lead.id);
        state.lead.splice(index, 1);
    },
    SEND_LEAD(state) {
        state.isLoading = true;
        state.errors = {};
    },
    SEND_LEAD_SUCCESS(state) {
        state.isLoading = false;
    },
    
    // construction
    UPDATE_CONSTRUCTION(state, construction) {
        state.isLoading = true;
    },
    UPDATE_CONSTRUCTION_SUCCESS(state) {
        state.errors = {};
        state.isLoading = false;
    },
    UPDATE_CONSTRUCTION_ERROR(state, errors) {          
        state.errors = errors.errors;
        state.isLoading = false;
    },
    FETCH_CONSTRUCTION(state, construction) {
        state.isLoading = true;
        state.errors = {};
    },
    FETCH_CONSTRUCTION_SUCCESS(state, construction) {
        state.lead = construction;
        state.isLoading = false;
    },
    FETCH_CONSTRUCTION_ERROR(state, errors) {
        state.errors = errors.errors;
        state.isLoading = false;
    },
    DELETE_CONSTRUCTION(state, construction) {
        let index = state.construction.findIndex(item => item.id === construction.id);
        state.construction.splice(index, 1);
    },
    SEND_CONSTRUCTION(state) {
        state.isLoading = true;
        state.errors = {};
    },
    SEND_CONSTRUCTION_SUCCESS(state) {
        state.isLoading = false;
    },

    // LEAD
    updateDotNumber (state, dot_number) {
        state.lead.dot_number = dot_number;
    },
    updateTelephone (state, phone) {
        state.lead.phone = phone;
    },
    updateLegalName (state, legal_name) {
        state.lead.legal_name = legal_name;
    },
    updateEmail (state, email_address) {
        state.lead.email_address = email_address;
    },
    updateStreet (state, phy_street) {
        state.lead.phy_street = phy_street;
    },
    updateZipCode (state, phy_zip) {
        state.lead.phy_zip = phy_zip;
    },
    updateNbrPowerUnit (state, nbr_power_unit) {
        state.lead.nbr_power_unit = nbr_power_unit;
    },
    updateLastInsuranceCarrier (state, last_insurance_carrier) {
        state.lead.last_insurance_carrier = last_insurance_carrier;
    },
    updateLastInsuranceDate (state, last_insurance_date) {
        state.lead.last_insurance_date = last_insurance_date;
    },
    updateInsuranceCancellationDate (state, insurance_cancellation_date) {
        state.lead.insurance_cancellation_date = insurance_cancellation_date;
    },
    updateCompany (state, dba_name) {
        state.lead.dba_name = dba_name;
    },
    updateCity (state, phy_city) {
        state.lead.phy_city = phy_city;
    },
    updateState (state, phy_state) {
        state.lead.phy_state = phy_state;
    },
    updateDriverTotal (state, driver_total) {
        state.lead.driver_total = driver_total;
    },
    updateDescription (state, description) {
        state.lead.description = description;
    },
    updateComment (state, comment) {
        state.lead.comment = comment;
    },
    updateDotSearch (state, dot_search) {
        state.dot_search = dot_search;
    },
    updatePhoneSearch (state, phone_search) {
        state.phone_search = phone_search;
    },
    updateFirstName (state, first_name) {
        state.lead.first_name = first_name;
    },
    updateLastName (state, last_name) {
        state.lead.last_name = last_name;
    },
    updateFullTimeEmployees (state, full_time_employees) {
        state.lead.full_time_employees = full_time_employees;
    },
    updatePartTimeEmployees (state, part_time_employees) {
        state.lead.part_time_employees = part_time_employees;
    },
    updateCurrentlyInsured (state, currently_insured) {
        state.lead.currently_insured = currently_insured;
    },
    updateYearsOfExperience (state, years_of_experience) {
        state.lead.years_of_experience = years_of_experience;
    },
    updateLegalEntity (state, legal_entity) {
        state.lead.legal_entity = legal_entity;
    },
    updateCoverageType (state, coverage_type) {
        state.lead.coverage_type = coverage_type;
    },
    
    // CONSTRUCTION
    updateLicenseNumber (state, licence_number) {
        state.lead.licence_number = licence_number;
    },
    updateLicenceSearch (state, licence_search) {
        state.licence_search = licence_search;
    },
    updateLicenceStatus (state, license_status) {
        state.lead.license_status = license_status;
    },
    updateScrapeName (state, scrape_name) {
        state.lead.scrape_name = scrape_name;
    },
    updateClassifications (state, classifications) {
        state.lead.classifications = classifications;
    },
    updateIssueDate (state, issue_date) {
        state.lead.issue_date = issue_date;
    },
    updateExpireDate (state, expire_date) {
        state.lead.expire_date = expire_date;
    },
    updateBondCarrier (state, bond_carrier) {
        state.lead.bond_carrier = bond_carrier;
    },
    updateBondEffectiveDate (state, bond_effective_date) {
        state.lead.bond_effective_date = bond_effective_date;
    },
    updateWorkersCompensationCarrier (state, workers_compensation_carrier) {
        state.lead.workers_compensation_carrier = workers_compensation_carrier;
    },
    updateWorkersCompensationEffectiveDate (state, workers_compensation_effective_date) {
        state.lead.workers_compensation_effective_date = workers_compensation_effective_date;
    },
    updateLiabilityInsuranceCarrier (state, liability_insurance_carrier) {
        state.lead.liability_insurance_carrier = liability_insurance_carrier;
    },
    updateLiabilityInsuranceExpirationDate (state, liability_insurance_expiration_date) {
        state.lead.liability_insurance_expiration_date = liability_insurance_expiration_date;
    },
    
}

export default mutations