let getters = {
    lead: state => {
        return state.lead;
    },
    construction: state => {
        return state.construction;
    },
    errors: state => {
        return state.errors;
    },
    isLoading: state => {
        return state.isLoading;
    }    
}

export default getters