<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::check()) {
        return view('/lead');
    } else {
        return view('auth.login');
    }
});

Auth::routes();

//Route::get('/login', 'Auth\LoginController@login')->name('login');
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/lead', 'LeadController@index')->name('lead')->middleware('can:edit_lead');
Route::get('/construction', 'ConstructionController@index')->name('construction')->middleware('can:edit_lead');

// Route::prefix('admin')->group(function () { // original
Route::prefix('admin')->middleware('auth')->group(function () { // redirect to login if logged out
//Route::prefix('admin')->middleware('can:register_user')->group(function () {

    //dd($role = Auth::user()->role);
    
    Route::get('/', function () {
        return redirect('/admin/dashboard');
    });
    Route::get('/dashboard', 'Admin\DashboardController@index')->name('admin.dashboard');
    Route::resource('/users','Admin\UserController');
    
    // Trucker leads
    Route::get('/leads/import', 'Admin\ImportLeadsController@index')->name('leads.import');
    Route::get('/import-leads', 'Admin\ImportLeadsController@index')->name('leads.import');  // Not really needed
    Route::post('/import-leads', 'Admin\ImportLeadsController@import')->name('import-leads');
    Route::get('/import-leads/progress', 'Admin\ImportLeadsController@progress')->name('leads.progress');
    Route::resource('/leads','Admin\LeadController');
    
    // Construction leads
    Route::get('/constructions/import', 'Admin\ImportConstructionsController@index')->name('constructions.import');
    Route::get('/import-constructions', 'Admin\ImportConstructionsController@index')->name('constructions.import');  // Not really needed
    Route::post('/import-constructions', 'Admin\ImportConstructionsController@import')->name('import-constructions');
    Route::get('/import-constructions/progress', 'Admin\ImportConstructionsController@progress')->name('constructions.progress');
    Route::resource('/constructions','Admin\ConstructionController');
    
    Route::resource('/brokers','Admin\BrokerController');
    Route::get('/recent-activities', 'Admin\ApiRequestLogController@index');
    Route::get('/email-log', 'Admin\SentEmailsLogController@index');
    Route::get('/import-error-log', 'Admin\ImportErrorLogController@index')->name('import-log');
    
    // Route::view('/ui', 'admin.ui');
});

Route::get('/clear-cache', 'ClearCacheController@index');
Route::get('/db-migrate', 'DbMigrateController@index')->middleware('can:register_user');
