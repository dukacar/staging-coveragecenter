<?php

namespace App\Imports;

use App\Construction;
use App\User;
use App\ImportErrorLog;
use Illuminate\Support\Collection;
//use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\Importable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\RemembersChunkOffset;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterImport;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Mail\ImportFinished;
use DB;
// use Illuminate\Support\Facades\Log;

class ConstructionsImport implements 
    //ToCollection, 
    WithHeadingRow,
    WithChunkReading,
    WithEvents,
    ShouldQueue

{
    use Importable;
    use RegistersEventListeners;
    use RemembersChunkOffset;

    public $jobId;
    public $importedBy;

    public function __construct(User $user) {

        set_time_limit(0);
        $this->importedBy = $user;
        $this->jobId = Str::random(10);
    }
    
    public function getJobId()
    {
        return $this->jobId;
    }
    
    public function chunkSize(): int
    {
        return 1000;
    }
    
    public function insertBulk(Int $row_number, Array $rows)
    {
        try {
            // $result = Lead::insert($rows); // Just inserts roes
            $result = Construction::upsert($rows, 'license_number'); // Inserts and updates rows at the same time
            return boolval($result);

        } catch(\Exception $e) {
            $endpos = strpos($e->getMessage(), '(SQL:');
            $error_message = substr($e->getMessage(), 0, $endpos - 1);
            try {
                $error_log = ImportErrorLog::create([
                    'job_id' => $this->jobId,
                    'row_number' => $row_number,
                    'message' => $error_message
                ]);
            } catch(\Exception $e) {
                error_log($e->getMessage());
            }
            
            return false;
        }
    }

    public static function afterImport(AfterImport $event)
    {
        $payload = $event->getConcernable();
        $errorCount = ImportErrorLog::where('job_id', $payload->jobId)->count();

        try {
            $email = new ImportFinished($payload, $errorCount);
            Mail::to($payload->importedBy)->send($email);
        } catch(\Exception $e) {
            error_log($e->getMessage());
        }
    }

     public function prepareForValidation($data, $index) //  NOT IN FUNCTION ANY MORE
     {
         $data['last_insurance_date'] = Carbon::parse($data['last_insurance_date'])->toDateTimeString();
         $data['insurance_cancellation_date'] = Carbon::parse($data['insurance_cancellation_date'])->toDateTimeString();
         $data['add_date_date'] = Carbon::parse($data['add_date_date'])->toDateTimeString();
        
         return $data;
     }
    
    public function truncate()
    {
        DB::table('constructions')->truncate();
        DB::table('import_error_logs')->truncate();
    }
}