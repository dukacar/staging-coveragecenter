<?php

namespace App;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class ApiRequestLog extends Model
{
    use Sortable;
    use Filterable;

    public $timestamps = true;

    public $sortable = [
        'id',
        'description',
        'response',
        'response_time',
        'created_at',
    ];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function lead()
    {
        if ($this->description == 'Trucking')
        {
            return $this->belongsTo('App\Lead');
        }
        else if ($this->description == 'Construction')
        {
            return $this->belongsTo('App\Construction');
        }
    }
}
