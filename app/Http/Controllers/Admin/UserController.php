<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeMail;
use App\Mail\UserEdited;
use App\Http\Controllers\Controller;
use App\User;
//use Config;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:register_user');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::with('roles')->sortable()
            ->filter($request->all())
            ->paginate(15);

        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'ends_with:coveragecenter.net', 'string', 'email', 'max:255'],
            'role' => ['required', 'string', 'in:admin,agent'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        */
        
        $validator = \Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'ends_with:coveragecenter.net', 'string', 'email', 'max:255'],
            'role' => ['required', 'string', 'in:admin,agent'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        
        // Try to create email account
        $result = $this->createEmailAccount($request);
        if (!$result['success'])
        {
            $error = \Illuminate\Validation\ValidationException::withMessages([
               'email' => [$result['error']],
            ]);
            notify()->error($result['error']);
            throw $error;
        }

        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'city' => $request['city'],
            'state' => $request['state'],
            'password' => Hash::make($request['password']),
        ]);

        $user->assignRole($request['role']);

        Mail::send(new WelcomeMail($user));

        notify()->success('User created!');

        return redirect()->route('users.index');
    }
    
    /**
     * Create email address account for user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function createEmailAccount(Request $request)
    {
        $cookie = "cookie-name";
        $url = env('CP_URL') . '/login/?login_only=1';
        $curl_request = [
            'user' => env('CP_USER'),
            'pass' => env('CP_PWD'),
            'goto_uri' => '/',
        ];
        $query = http_build_query($curl_request);

        // Create new curl handle
        $ch = curl_init();
        //curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_COOKIESESSION, true);
        //curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        curl_setopt($ch, CURLOPT_TIMEOUT, 100020);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // Execute the curl handle and fetch info then close streams.
        $response = curl_exec($ch);
        $response = json_decode($response, true);
        // echo "response:<pre>";
        // print_r($response);
        // echo "</pre>";
        if (isset($response['security_token']))
        {
            $url = env('CP_URL') . $response['security_token'] . '/execute/Email/add_pop';
            $curl_request = [
                'email' => substr($request['email'], 0, strrpos($request['email'], '@')),
                'domain' => 'coveragecenter.net',
                'password' => $request['password'],
                'quota' => 2048,
                'send_welcome_email' => 0
            ];
            $query = http_build_query($curl_request);
            
            //another request preserving the session
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
            $response = curl_exec($ch);
            $response = json_decode($response, true);
            //echo "answer:<pre>";
            //print_r($answer);
            //echo "</pre>";
            if (!empty($response['errors']))
            {
                $response = [
                    'success' => false,
                    'error' => $response['errors'][0]
                ];
            }
            else
            {
                $response = [
                    'success' => true,
                    'message' => 'Mailbox successfully created.'
                ];
            }
        }
        else
        {
            $response = [
                'success' => false,
                'error' => 'Can\'t access mailbox creation API.'
            ];
        }
        curl_close($ch);
        
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::where('id', $id)->with('roles')->first();
        $user = json_decode($data);

        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'role' => ['required', 'string', 'in:admin,agent'],
            'password' => ['nullable', 'string', 'min:8', 'confirmed'],
        ]);
        
        $old_email = $user->email;
        $new_email = $request->get('email');
        if ($old_email !== $new_email)
        {
            if ($request->get('password') == '')
            {
                $error = \Illuminate\Validation\ValidationException::withMessages([
                   'password' => ['You need to specify password if user\'s email is changed.'],
                ]);
                notify()->error('You need to specify password if user\'s email is changed.');
                throw $error;
            }
            
            // Delete old email account
            $result = $this->deleteEmailAccount($user);
            
            // Create new email account
            $result = $this->createEmailAccount($request);
            if (!$result['success'])
            {
                $error = \Illuminate\Validation\ValidationException::withMessages([
                   'email' => [$result['error']],
                ]);
                notify()->error($result['error']);
                throw $error;
            }
        }

        if ($request->get('password') == '') {
            $user->update ($request->except('password'));
        } else {
            $user->update($request->all());
        }

        $user->assignRole($request['role']);

        Mail::send(new UserEdited($user)); 

        notify()->success('User updated!');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $result = $this->deleteEmailAccount($user);
        
        if (!$result['success'])
        {
            $error = \Illuminate\Validation\ValidationException::withMessages([
               'email' => [$result['error']],
            ]);
            notify()->error($result['error']);
            throw $error;
        }
        else
        {
            $user->delete();
            notify()->success('User deleted!');
            return redirect()->route('users.index');
        }
    }
    
    /**
     * Create email address account for user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function deleteEmailAccount(User $user)
    {
        $cookie = "cookie-name";
        $url = env('CP_URL') . '/login/?login_only=1';
        $request = [
            'user' => env('CP_USER'),
            'pass' => env('CP_PWD'),
            'goto_uri' => '/',
        ];
        $query = http_build_query($request);

        // Create new curl handle
        $ch = curl_init();
        //curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_COOKIESESSION, true);
        //curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        curl_setopt($ch, CURLOPT_TIMEOUT, 100020);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // Execute the curl handle and fetch info then close streams.
        $response = curl_exec($ch);
        $response = json_decode($response, true);
        // echo "response:<pre>";
        // print_r($response);
        // echo "</pre>";
        if (isset($response['security_token']))
        {
            $url = env('CP_URL') . $response['security_token'] . '/execute/Batch/strict';
            $request = [
                'command' => '["Email","delete_pop",{"email":"' . $user->email . '"}]'
            ];
            $query = http_build_query($request);
            
            //another request preserving the session
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
            $response = curl_exec($ch);
            $response = json_decode($response, true);
            //echo "response:<pre>";
            //print_r($response);
            //echo "</pre>";
            //die("UNMIREM");
            
            if (!empty($response['errors']))
            {
                $response = [
                    'success' => false,
                    'error' => $response['errors'][0]
                ];
            }
            else
            {
                $response = [
                    'success' => true,
                    'message' => 'Mailbox successfully created.'
                ];
            }
        }
        else
        {
            $response = [
                'success' => false,
                'error' => 'Can\'t access mailbox creation API.'
            ];
        }
        curl_close($ch);
        
        return $response;
    }
}
