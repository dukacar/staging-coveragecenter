<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Construction;

class ConstructionController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:register_user');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $constructions = Construction::sortable()
            ->filter($request->all())
            ->paginate(15);

        return view('admin.construction.index', compact('constructions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $states = config('constants.states');
        $legal_entities = config('constants.legal_entities');
        $coverage_types = config('constants.coverage_types');

        return view('admin.construction.create', compact('states', 'legal_entities', 'coverage_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'legal_name' => ['required', 'string', 'max:255'],
            'email_address' => ['nullable', 'string', 'email', 'max:255'],
            'phy_city' => ['required', 'string', 'max:255'],
            'phy_zip' => ['required', 'numeric', 'digits:5'],
            'phy_state' => ['required', 'string', 'max:255'],
            'first_name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'numeric', 'digits:10'],
            'currently_insured' => ['required', 'string'],
        ]);

        $construction = Construction::create($request->all());

        notify()->success('Construction lead created!');

        return redirect()->route('constructions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $states = config('constants.states');
        $legal_entities = config('constants.legal_entities');
        $coverage_types = config('constants.coverage_types');

        $data = Construction::where('id', $id)->first();
        $construction = json_decode($data);

        return view('admin.constructions.edit', compact('construction', 'states', 'legal_entities', 'coverage_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Construction $construction)
    {
        $request->validate([
            'legal_name' => ['required', 'string', 'max:255'],
            'email_address' => ['nullable', 'string', 'email', 'max:255'],
            'phy_city' => ['required', 'string', 'max:255'],
            'phy_zip' => ['required', 'numeric', 'digits:5'],
            'phy_state' => ['required', 'string', 'max:255'],
            'first_name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'numeric', 'digits:10'],
            'currently_insured' => ['required', 'string'],
        ]);

        try {
            $construction->update($request->all());
            notify()->success('Construction lead updated!');
        } catch (\Exception $error) {
            dd($error);
        }

        return redirect()->route('constructions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Construction $construction)
    {
        $construction->delete();
        notify()->success('Construction lead deleted!');

        return redirect()->route('constructions.index');
    }
}
