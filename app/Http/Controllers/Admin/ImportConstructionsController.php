<?php

namespace App\Http\Controllers\Admin;

use App\Imports\ConstructionsImport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Construction;

use App\ImportErrorLog;
use Session;
use Response;
use ZipArchive;
use DB;

class ImportConstructionsController extends Controller
{
    public $row_number = 0;
    protected $jobId = '';
    
    public function __construct()
    {
        $this->middleware('can:register_user');
    }

    public function index()
    {
        // Clear all files in dir
        $this->clearDir();
        return view('admin.construction.import');
    }
    
    public function progress(Request $request) 
    {
        $progress = $this->getProgress();
        return response()->json(array($progress));
    }

    public function import(Request $request) 
    {   
        set_time_limit(1500);
        
        $user = auth()->user();
        
        if ($request->hasFile('file'))
        {
            // Create directory if not exists
            if (!is_dir(public_path('files'))) {
                mkdir(public_path('files'), 0755, true);
            }

            // Get file extension
            $extension = $request->file('file')->getClientOriginalExtension();

            // Valid extensions
            $validextensions = ["csv", "zip", "sql"];
            
            // Valid extensions inside zip file
            $valid_zip_extensions = ["csv", "sql"];

            // Check extension
            if (in_array(strtolower($extension), $validextensions)) {
                
                // Rename file
                $file = $request->file('file')->getClientOriginalName();
                // $file = $file_name; // . '.' . request()->file->getClientOriginalExtension();
                    
                // Uploading file to given path
                $request->file('file')->move(public_path('files'), $file);
                
                // Check if zip
                if (strtolower($request->file('file')->getClientOriginalExtension()) == 'zip')
                {
                    $zip = new ZipArchive();
                    $file = public_path('files') . DIRECTORY_SEPARATOR . $file;
                    $status = $zip->open($file);
                    if ($status !== true) {
                        header("HTTP/1.0 400 Bad Request");
                        echo "Error opening file!";
                    } else {
                        // Check if files are cvs
                        for ($i = 0; $i < $zip->numFiles; $i++) {
                             $filename = $zip->getNameIndex($i);
                             if (!in_array(strtolower(pathinfo($filename, PATHINFO_EXTENSION)), $valid_zip_extensions))
                             {
                                header("HTTP/1.0 400 Bad Request");
                                echo $filename . " in zip file is not a CSV file!";
                             }
                         }
                        $storageDestinationPath = public_path('files') . DIRECTORY_SEPARATOR;
                        if (!\File::exists($storageDestinationPath)) {
                            \File::makeDirectory($storageDestinationPath, 0755, true);
                        }
                        $success = $zip->extractTo($storageDestinationPath);
                        $zip->close();
                        @unlink($file);
                        if (!$success)
                        {
                            header("HTTP/1.0 400 Bad Request");
                            echo "Error unziping file!";
                        }
                    }
                }
            }
            
            exit;
        }
        
        if ($request->isMethod('post') && isset($request->submited) && !empty($request->submited))
        {
            $total_start_time = microtime(true);
            $files = $this->getFiles();
            
            if (empty($files))
            {
                return back()->withErrors('No files have been uploaded.');
            }
            
            // Check files extensions
            foreach ($files as $file)
            {
                if (strtolower(pathinfo($file, PATHINFO_EXTENSION)) !== 'csv')
                {
                    return back()->withErrors($file . ' is not a CSV file, processing abandoned.');
                }
            }
            
            $import = new ConstructionsImport($user);
            $this->jobId = $import->getJobId();
            
            if (!empty($request->empty_records))
            {
                // Truncate constructions table
                $import->truncate();
            }
            
            DB::connection()->disableQueryLog();
            $dispatcher = DB::connection()->getEventDispatcher();
            DB::connection()->unsetEventDispatcher();
            
            $logs = [];
            foreach ($files as $file)
            {
                $logs[$file] = $this->processFile($file, $import);
            }
            
            DB::connection()->enableQueryLog();
            DB::connection()->setEventDispatcher($dispatcher);
            unset($dispatcher);
            
            // Remove uploaded files
            $this->clearDir();
            
            // Create response message
            $total = [
                'count' => 0,
                'inserted' => 0,
                'errors' => 0,
            ];
            $response = 'Import finished.';
            foreach ($logs as $file => $log)
            {
                $response .= '<br>File: <b>' . $file . '</b>, rows: ' . $log['count'] . ', inserted: ' . $log['inserted'] . ', errors: ' . $log['errors'] . ', time: ' . $log['time'] . ' sec.';
                $total['count'] += $log['count'];
                $total['inserted'] += $log['inserted'];
                $total['errors'] += $log['errors'];
            }
            $total_time = intval(microtime(true) - $total_start_time);
            $response .= '<br><b>Total</b>: rows: <b>' . $total['count'] . '</b>, inserted: <b>' . $total['inserted'] . '</b>, errors: <b>' . $total['errors'] . '</b>, time: <b>' . $total_time . '</b> sec.';
            return back()->withStatus($response);
        }
        
        // $file = $request->file('file')->store('import');

        //$user = auth()->user();
        //$import = new LeadsImport($user);
        // $import->queue($file);
           
        //return back()->withStatus('Import in queue, email will be sent to ' . $user->email . ' when import is finished.');
    }
    
    protected function formatBytes($bytes, $precision = 2)
    {
        $units = array("b", "kb", "mb", "gb", "tb");

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        $bytes /= (1 << (10 * $pow));

        return round($bytes, $precision) . " " . $units[$pow];
    }
    
    protected function clearDir()
    {
        $files = glob(public_path('files') . DIRECTORY_SEPARATOR . '*'); // get all file names
        foreach($files as $file){ // iterate files
            if(is_file($file)) {
                unlink($file); // delete file
            }
        }
    }
    
    protected function saveProgress($payload)
    {
        $file = public_path('files') . DIRECTORY_SEPARATOR . 'progress.txt';
        file_put_contents($file, $payload);
    }
    
    protected function getProgress()
    {
        $file = public_path('files') . DIRECTORY_SEPARATOR . 'progress.txt';
        if (file_exists($file))
        {
            $f = fopen($file, 'r');
            if ($f && filesize($file) > 0) {
                $contents = fread($f, filesize($file));
                fclose($f);
                return $contents;
            }
            else
            {
                return 0;
            }
        }
        return 0;
    }
    
    protected function getFiles()
    {
        $path = public_path('files');
        $files = array_diff(scandir($path), ['.', '..']);
        return $files;
    }
    
    protected function processFile($file, $import)
    {
        $start_time = microtime(true);
        
        $file_name = $file;
        $progress = [
            'message' => 'Preparing data from file: <b>' . $file_name . '</b>',
            'file_name' => $file_name,
            'lines_count' => 0,
            'row_number' => 0,
            'percentage' => 0,
            'start_time' => $start_time,
        ];
        $this->saveProgress(json_encode($progress));
        
        $lines_count = 0;
        $inserted = 0;
        $errors = 0;
        $file = public_path('files') . DIRECTORY_SEPARATOR . $file;
        if (file_exists($file))
        {
            $fileObj = new \SplFileObject($file);
            $fileObj->setFlags(
                \SplFileObject::READ_CSV 
              | \SplFileObject::SKIP_EMPTY 
              | \SplFileObject::READ_AHEAD 
              | \SplFileObject::DROP_NEW_LINE
            );
            $fileObj->setCsvControl(',');
            
            $lines_count = iterator_count($fileObj) - 1;

            $this->row_number = 0;
            $count = 0;
            $chunk_size = $import->chunkSize();
            if ($chunk_size > $lines_count)
            {
                $chunk_size = $lines_count;
            }
            
            foreach($fileObj as $key => $row)
            {
                if ($key == 0)
                {
                    // Get header
                    $header = $row;
                    continue;
                }
                
                $this->row_number++;
                $count++;

                if ($this->row_number % 10 == 0)
                {
                    $percentage = round($this->row_number * 100 / $lines_count);
                    $message = '';
                    $message .= 'Processing file: <b>' . $file_name . '</b>, ';
                    $message .= 'rows: <b>' . $this->row_number . '</b> of <b>' . $lines_count . '</b> ';
                    $message .= '(<b>' . $percentage . '%</b>)';
                    $progress = [
                        'message' => $message,
                        'file_name' => $file_name,
                        'lines_count' => $lines_count,
                        'row_number' => $this->row_number,
                        'percentage' => $percentage,
                        'start_time' => $start_time,
                    ];
                    $this->saveProgress(json_encode($progress));
                }
                
                unset($lead_row);
                $lead_row = [];
                // Maybe some check if length of arrays are the same.
                foreach ($header as $key => $value)
                {
                    try {
                        $lead_row[$value] = $row[$key];
                    } catch(\Exception $e) {
                        $errors++;
                        $message = $e->getMessage();
                        if (str_starts_with($e->getMessage(), 'Undefined array key'))
                        {
                            $message = str_replace('Undefined array key', '', $e->getMessage());
                            $message = intval(trim($message));
                            $message = 'Undefined array key: ' . $header[$message];
                        }
                        ImportErrorLog::create([
                            'job_id' => $this->jobId,
                            'row_number' => $this->row_number,
                            'message' => $message . "\n" . json_encode($row)
                        ]);
                        continue 2;
                    } catch(\Exception $e) {
                        error_log($e->getMessage());
                        continue 2;
                    }
                }
                
                // $import->insert($this->row_number, $lead_row); # Instert one by one

                $lead_row_temp = $lead_row;
                //$lead_row_temp['add_date_date'] = now();
                $lead_row_temp['title'] = !empty($lead_row_temp['title']) ? intval($lead_row_temp['title']) : null;
                //$lead_row_temp['legal_entity'] = !empty($lead_row_temp['legal_entity']) ? $lead_row_temp['legal_entity'] : null;
                
                if (empty($lead_row_temp['id']) && !empty($lead_row_temp['license_number']))
                {
                    $lead_row_temp['id'] = $lead_row_temp['license_number'];
                }
                
                $lead_rows[] = $lead_row_temp;
                if (count($lead_rows) === $chunk_size || $lines_count === $this->row_number || $fileObj->eof())
                {
                    $result = $import->insertBulk($this->row_number, $lead_rows); # Instert bulk
                    if ($result)
                    {
                        $inserted += count($lead_rows);
                    }
                    else
                    {
                        $errors += count($lead_rows);
                    }
                    
                    unset($lead_rows);
                    $lead_rows = [];
                }
            }
            
            return [
                'count' => $this->row_number,
                'inserted' => $inserted,
                'errors' => $errors,
                'time' => intval(microtime(true) - $start_time),
            ];
        }
        else
        {
            ImportErrorLog::create([
                'job_id' => 1,
                'row_number' => 1,
                'message' => 'File doesnt exist!'
            ]);
            return [
                'count' => $this->row_number,
                'inserted' => $inserted,
                'errors' => $errors,
                'time' => 0,
            ];
        }
    }
}
