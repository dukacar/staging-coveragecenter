<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Construction;
use Carbon\Carbon;

class ConstructionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('construction');
    }

    public function get(Request $id)
    {
        $term = $id->json()->all();
        $key = array_key_first($term);
        $value = reset($term);

        $construction = Construction::where($key, $value)->first();
        return response()->json($construction);
    }

    public function store($id, Request $request)
    {
        $request->validate([
            'legal_name'                          => 'required',
            'first_name'                          => 'required',
            // 'last_name'                           => 'required',
            'phone'                               => 'required|digits:10',
            'phy_city'                            => 'required',
            'phy_zip'                             => 'required|digits:5',
            'phy_state'                           => 'required|max:2',
            // 'phy_street'                          => 'required',
            'legal_entity'                        => 'nullable|string',
            'issue_date'                          => 'nullable|date',
            'expire_date'                         => 'nullable|date',
            'license_status'                      => 'nullable|string',
            'classifications'                     => 'nullable|string',
            'bond_carrier'                        => 'nullable|string',
            'bond_effective_date'                 => 'nullable|date',
            'workers_compensation_carrier'        => 'nullable|string',
            'workers_compensation_effective_date' => 'nullable|date',
            'liability_insurance_carrier'         => 'nullable|string',
            'liability_insurance_expiration_date' => 'nullable|date',
            'coverage_type'                       => 'nullable|string',
            'email_address'                       => 'nullable|email',
            'comment'                             => 'nullable|string',
        ]);

        $lead = Construction::where('id', $id)->update([
            'legal_name'                          => $request->legal_name,
            'first_name'                          => $request->first_name,
            'last_name'                           => $request->last_name,
            'scrape_name'                         => $request->scrape_name,
            'phone'                               => $request->phone,
            'phy_city'                            => $request->phy_city,
            'phy_zip'                             => $request->phy_zip,
            'phy_state'                           => $request->phy_state,
            'phy_street'                          => $request->phy_street,
            'legal_entity'                        => $request->legal_entity,
            'issue_date'                          => $request->issue_date,
            'expire_date'                         => $request->expire_date,
            'license_status'                      => $request->license_status,
            'classifications'                     => $request->classifications,
            'bond_carrier'                        => $request->bond_carrier,
            'bond_effective_date'                 => $request->bond_effective_date,
            'workers_compensation_carrier'        => $request->workers_compensation_carrier,
            'workers_compensation_effective_date' => $request->workers_compensation_effective_date,
            'liability_insurance_carrier'         => $request->liability_insurance_carrier,
            'liability_insurance_expiration_date' => $request->liability_insurance_expiration_date,
            'coverage_type'                       => $request->coverage_type,
            'email_address'                       => $request->email_address,
            'comment'                             => $request->comment,
        ]);

        return response()->json($lead);
    }

    public function delete($id)
    {
        Construction::destroy($id);

        return response()->json("ok");
    }
}

